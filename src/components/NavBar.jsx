import React from "react";
import profile from "../images/profile.jpg";
import { AiFillPhone, AiFillMail, AiOutlineLink } from "react-icons/ai";
import { Container } from "react-bootstrap";
function NavBar() {
  return (
    <div className={"d-flex justify-content-between flex-direction-column"}>
      <Container>
      <div className={"d-flex flex-direction-column"}>
        <img style={{borderRadius: "50%"}} src={profile} alt="" width="250" height="200" />
        <h1>Seanghay</h1>
        <h2>Huort</h2>
        <h3>Jobs</h3>
      </div>
      <div className={"d-flex"}>
        <div className={"d-flex"}>
          <AiFillPhone />
          <p>068396398</p>
        </div>
        <div className={"d-flex"}>
          <AiFillMail />
          <p>haykean0303@gmail.com</p>
        </div>
        <div className={"d-flex"}>
          <AiOutlineLink />
          <p>hafjhakjfh.net</p>
        </div>
      </div>
      </Container>
    </div>
  );
}

export default NavBar;
